<?php
/**
 * Created by PhpStorm.
 * User: kofi
 * Date: 2/15/2018
 * Time: 6:48 PM
 */

namespace App\Helper;


class StatusCodes
{
    public static $success = 200;
    public static $error = 422;
}