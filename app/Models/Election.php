<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Election extends Model
{
    protected $guarded = ['id'];
    
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function categories(){
        return $this->belongsToMany('App\Models\Category', 'category_elections');
    }

    public function portfolios()
    {
        return $this->hasMany('App\Models\Portfolio');
    }
}
