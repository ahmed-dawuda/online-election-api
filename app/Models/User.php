<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * setters
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    /**
     * getters
     */
    public function getImageAttribute($value)
    {
        return asset('storage/'.$value);
    }

    public function getFirstnameAttribute($value)
    {
        return ucfirst($value);
    }

    public function getSurnameAttribute($value)
    {
        return ucfirst($value);
    }

    public function categories()
    {
        return $this->hasMany('App\Models\Category');
    }

    public function voterCategories()
    {
        return $this->belongsToMany('App\Models\Category', 'categorizations', 'user_id', 'category_id');
    }

    public function voters(){
        $voters_id = Voter::where('ec_id', $this->id)->pluck('voter_id');
        return User::whereIn('id', $voters_id)->get();
    }

    public function elections()
    {
        if ($this->admin) {
            return $this->hasMany('App\Models\Election');
        }else {
            return [];
        }
    }
}
