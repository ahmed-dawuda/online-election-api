<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = ['id'];

    public function getNameAttribute($value)
    {
        return ucfirst($value);
    }

    public function getDescriptionAttribute($value)
    {
        return ucfirst($value);
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
    
    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'categorizations', 'category_id', 'user_id');
    }
    
    public function elections()
    {
        return $this->belongsToMany('App\Models\Election', 'category_elections');
    }
}
