<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
    protected $guarded = ['id'];
    
    public function election()
    {
        return $this->belongsTo('App\Models\Election');
    }
    
    public function candidates()
    {
        return $this->hasMany('App\Models\Candidate');
    }
    
    public function getCandidates()
    {
        $voter_ids = $this->candidates()->pluck('voter_id');
        $voters = User::whereIn('id', $voter_ids)->get();
        return $voters;
    }
}
