<?php

namespace App\Http\Controllers\Api;

use App\Helper\StatusCodes;
use App\Repository\Category\CategoryService;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    protected $category;
    
    public function __construct(CategoryService $service)
    {
        $this->category = $service;
    }

    public function index(Request $request)
    {
        return Auth::user()->categories()->paginate(9);
    }

    public function delete(Request $request)
    {
//        return $request->id;
        $this->category->deleteById($request->id);

        return response()->json([
            'status'=> StatusCodes::$success,
            'statusText' => 'Deletion successful'
        ], StatusCodes::$success);
    }

    public function show(Request $request)
    {
        return Auth::user()->categories()->orderBy('name', 'ASC')->get();
    }

    public function create(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'required|unique:categories',
            'priority' => 'required|numeric',
            'description' => 'required'
        ]);
        
        if ($validator->fails()) {
            return response()->json([
                'status' => StatusCodes::$error,
                'statusText' => 'Validation errors',
                'errors' => $validator->errors()
            ], StatusCodes::$error);
        }

        $cat = $this->category->create([
            'name' => $data['name'],
            'description' => $data['description'],
            'priority' => $data['priority'],
            'user_id' => Auth::user()->id
        ]);

        return response()->json([
            'status' => StatusCodes::$success,
            'statusText' => 'OK',
            'data' => $cat
        ], StatusCodes::$success);
        
    }

    public function update(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'required',
            'priority' => 'required|numeric',
            'description' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => StatusCodes::$error,
                'statusText' => 'Validation errors',
                'errors' => $validator->errors()
            ], StatusCodes::$error);
        }
        
        $this->category->update($data);
        return response()->json([
            'status' => StatusCodes::$success,
            'statusText' => 'OK',
            'errors' => []
        ], StatusCodes::$success);
    }
}
