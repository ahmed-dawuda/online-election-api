<?php

namespace App\Http\Controllers\Api;

use App\Helper\StatusCodes;
use App\Models\Candidate;
use App\Models\Election;
use App\Models\Portfolio;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PortfolioController extends Controller
{
    public function create(Request $request)
    {
        $port = new Portfolio();
        $port->name = $request->name;
        $port->election_id = $request->election_id;
        $port->save();
        return response()->json(['status'=> true, 'data'=> ['portfolio'=> $port, 'candidates'=> []]], StatusCodes::$success);
    }

    public function portfolios(Request $request)
    {
        $portfolios = Portfolio::where('election_id', $request->election_id)->get();
        $result = [];
        foreach ($portfolios as $portfolio) {
            $result[] = ['portfolio' => clone $portfolio, 'candidates'=> $portfolio->getCandidates()];
        }
        return response()->json(['status'=> true, 'data'=> $result], StatusCodes::$success);
    }

    public function onlyPortfolio(Request $request)
    {
        $portfolios = Portfolio::where('election_id', $request->election_id)->get();
        return response()->json(['status'=> true, 'data'=> $portfolios], StatusCodes::$success);
    }

    public function addUser(Request $request)
    {
//        if(Election::find($request->election_id)->active) return;
//        return $request->all();
        $voter = User::where('email', $request->user)->orWhere('phone', $request->user)->get()->first();
//        return $voter;
        $count = Candidate::where('voter_id', $voter->id)->where('election_id', $request->election_id)->count();
//        return $count;

        if ($count){
            return response()->json(['status'=> false], StatusCodes::$success);
        }

        $candidate = new Candidate();
        $candidate->voter_id = $voter->id;
        $candidate->portfolio_id = $request->portfolio_id;
        $candidate->election_id = $request->election_id;
        $candidate->save();
        return response()->json(['status'=> true, 'data'=> $voter], StatusCodes::$success);
    }
}
