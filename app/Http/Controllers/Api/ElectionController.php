<?php

namespace App\Http\Controllers\Api;

use App\Helper\StatusCodes;
use App\Models\CategoryElection;
use App\Models\Election;
use App\Models\Portfolio;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ElectionController extends Controller
{
    public function create(Request $request)
    {
//        return $request->all();
        $election = Election::create([
            'name' => $request->name,
            'description' => $request->description,
            'date' => $request->date,
            'user_id' => Auth::user()->id
        ]);

        foreach ($request->categories as $category) {
            CategoryElection::create([
                'election_id' => $election->id,
                'category_id' => $category
            ]);
        }

        return response()->json([
            'status' => true,
            'statusText' => 'Election created successfully',
            'data' => []
        ]);
    }

    public function elections(Request $request)
    {
        if ($request->has('term')) {

        } else {
            return response()->json(['data' => Auth::user()->elections]);
        }
    }

    public function electionDetail(Request $request)
    {
        $portfolio = Portfolio::find($request->portfolio_id);
        $cadidates = $portfolio->getCandidates();
        return response()->json(['status'=> true, 'data'=> ['portfolio'=> $portfolio, 'candidates' => $cadidates]], StatusCodes::$success);
    }
}
