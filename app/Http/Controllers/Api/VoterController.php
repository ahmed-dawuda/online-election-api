<?php

namespace App\Http\Controllers\Api;

use App\Helper\StatusCodes;
use App\Models\Categorization;
use App\Models\Category;
use App\Models\User;
use App\Repository\User\UserService;
use App\Repository\Voter\VoterService;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VoterController extends Controller
{
    protected $userRepo;
    protected $voterRepo;

    public function __construct(UserService $userService, VoterService $voterService)
    {
        $this->userRepo = $userService;
        $this->voterRepo = $voterService;
    }

    public function voters(){

       return response()->json([
           'status'=> StatusCodes::$success,
           'statusText' => 'Ok',
           'data'=> ['voters' => Auth::user()->voters(), 'categories' => Auth::user()->categories]]);
    }

    public function search(Request $request)
    {
        $voter_ids = Categorization::where('category_id', $request->category)->pluck('user_id');
//        return $voter_ids;

        $voters = User::whereIn('id', $voter_ids)
            ->orWhere('firstname', $request->term)
            ->orWhere('surname', $request->term)
            ->orWhere('email', $request->term)
            ->orWhere('phone', $request->term)
            ->get();
        return $voters;
    }
}
