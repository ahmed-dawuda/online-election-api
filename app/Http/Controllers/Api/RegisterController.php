<?php

namespace App\Http\Controllers\Api;

use App\Helper\StatusCodes;
use App\Models\Categorization;
use App\Models\Voter;
use App\Repository\User\UserService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Validator;

class RegisterController extends Controller
{
    protected $user;

    public function __construct(UserService $service)
    {
        $this->user = $service;
    }
    
    public function register(Request $request)
    {
        $is_voter = url()->current() == route('register_voter') ? true : false;
        if ($is_voter) {
            $user = $this->user->getByAttributes(['email'=> $request->email, 'phone'=> $request->phone]);
            if ($user->count()) {
                return response()->json([
                    'status'=> false,
                    'statusText' => 'This voter already exist',
                    'voters' => $user
                ]);
            }
        }

        $rules = [
            'firstname' => 'required',
            'surname' => 'required',
            'gender' => 'required',
            'email' => 'required|email|unique:users',
            'phone' => 'required|numeric|unique:users',
            'dateOfBirth' => 'required'
        ];


        $imageFile = null;

        if ($is_voter) {
//            $rules['image'] = 'required';
//            $rules['dateOfBirth'] = 'required';
            $imageFile = $request->file('image');
        } else {
            $rules['password'] = 'required';
        }

//        return $rules;

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => StatusCodes::$error,
                'statusText' => 'Validation errors',
                'errors' => $validator->errors()
            ], StatusCodes::$error);
        }

        $voter = $this->createUser($request->all(), $is_voter, $imageFile);

        if ($is_voter) {
            foreach ($request->category as $category){
                $categorization = new Categorization();
                $categorization->user_id = $voter->id;
                $categorization->category_id = $category;
                $categorization->save();
            }
        }

        return response()->json([
            'status' => true,
            'statusText' => 'Registration Successful',
            'data' => [],
            'message' => 'A verification link has been sent to your email'
        ], StatusCodes::$success);
    }

    public function createUser(array $request, $is_voter, $image)
    {
        $user = [
            'firstname' => $request['firstname'],
            'surname' => $request['surname'],
            'gender' => $request['gender'],
            'phone' => $request['phone'],
            'email' => $request['email'],
            'dateOfBirth' => $request['dateOfBirth'],
            'admin' => !$is_voter
        ];
//        $generated_password = substr(uniqid('', true), -5);
        $generated_password = 'secret';

        if ($is_voter) {
            if ($image != null) {
                $filename = $request['phone'].time().'.'.$image->getClientOriginalExtension();
                $user['image'] = $filename;
                Storage::disk('local')->put('public/'.$filename, file_get_contents($image));
            }
//            $user['dateOfBirth'] = $request['dateOfBirth'];
            $user['password'] = $generated_password;
        } else {
            $user['password'] = $request['password'];
        }

        $voter = $this->user->create($user);

//        return Auth::user()->id;
        if ($is_voter) {
           Voter::create(['voter_id'=> $voter->id, 'ec_id' => Auth::user()->id]);
        }
        return $voter;
    }
}
