<?php
/**
 * Created by PhpStorm.
 * User: kofi
 * Date: 2/15/2018
 * Time: 5:39 PM
 */

namespace App\Repository\User;


interface UserService
{
    public function create(array $attributes);
    public function getByAttributes(array $attributes);
}