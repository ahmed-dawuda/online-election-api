<?php
namespace App\Repository\User;

use App\Models\User;
use App\Repository\User\UserService;

class UserRepo implements UserService
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function create(array $attributes)
    {
        $u = $this->user->create($attributes);
        return $u;
    }

    public function getByAttributes(array $attributes)
    {
        $userData = $this->user;
        foreach ($attributes as $key => $value) {
            $userData = $userData->orWhere($key, $value);
        }
//        $userData = $this->user->get();
        return $userData->get();
    }
}