<?php
namespace App\Repository\Category;


use App\Models\Category;

class CategoryRepo implements CategoryService
{
    protected $categoryModel;

    public function __construct(Category $category)
    {
        $this->categoryModel = $category;
    }

    public function create(array $attributes)
    {
        $category = $this->categoryModel->create($attributes);
        return $category;
    }

    public function update(array $attributes)
    {
        $category = $this->categoryModel->find($attributes['id']);
        $category->update($attributes);
    }

    public function getAll()
    {
        $this->categoryModel->paginate(9);
    }

    public function deleteById($id)
    {
        $this->categoryModel->destroy($id);
    }
}