<?php
namespace App\Repository\Category;


interface CategoryService
{
    public function create(array $attributes);
    public function update(array $attributes);
    public function getAll();
    public function deleteById($id);
}