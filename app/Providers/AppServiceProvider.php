<?php

namespace App\Providers;

use App\Repository\Category\CategoryRepo;
use App\Repository\Category\CategoryService;
use App\Repository\User\UserRepo;
use App\Repository\User\UserService;
use App\Repository\Voter\VoterRepo;
use App\Repository\Voter\VoterService;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(UserService::class, UserRepo::class);
        $this->app->singleton(CategoryService::class, CategoryRepo::class);
        $this->app->singleton(VoterService::class, VoterRepo::class);
    }
}
