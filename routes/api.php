<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/register', 'Api\RegisterController@register')->name('register_ec');
Route::middleware('auth:api')->post('/register/voter', 'Api\RegisterController@register')->name('register_voter');


Route::group(['prefix'=> 'category', 'middleware' => 'auth:api'], function () {
    Route::get('paginate', 'Api\CategoryController@index');
    Route::get('', 'Api\CategoryController@show');
    Route::post('', 'Api\CategoryController@create');
    Route::put('', 'Api\CategoryController@update');
    Route::delete('', 'Api\CategoryController@delete');
});

Route::group(['prefix'=> 'voter', 'middleware'=> 'auth:api'], function () {
    Route::get('voters', 'Api\VoterController@voters');
    Route::get('search', 'APi\VoterController@search');
});

Route::group(['prefix'=> 'election', 'middleware' => 'auth:api'], function () {
    Route::post('', 'Api\ElectionController@create');
    Route::get('', 'Api\ElectionController@elections');
    Route::get('detail', 'Api\ElectionController@electionDetail');
});

Route::group(['prefix' => 'portfolio', 'middleware' => 'auth:api'], function ()  {
    Route::post('', 'Api\PortfolioController@create');
    Route::get('', 'Api\PortfolioController@portfolios');
    Route::post('add-user', 'Api\PortfolioController@addUser');
    Route::get('only-portfolio', 'Api\PortfolioController@onlyPortfolio');
});